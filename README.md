# nix

All things nix and nix configurations

To symlink:

```bash
sudo ln  -s /home/billewanick/workspace/nix-laptop-config/configuration.nix      /etc/nixos/
     # Verify it worked
     ls  -l /etc/nixos
     cat    /etc/nixos/configuration.nix

ln -s /home/billewanick/workspace/nix-laptop-config/home.nix    ~/.config/nixpkgs
# Verify it worked
ls -l ~/.config/nixpkgs/
cat   ~/.config/nixpkgs/home.nix
```