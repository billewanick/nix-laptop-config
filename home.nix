{ config, pkgs, lib, ... }:

{
  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "20.09";

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "demo";
  home.homeDirectory = "/home/demo";

  home.packages = with pkgs; [
    # google-chrome
    tor-browser-bundle-bin
    spotify
    # elm2nix
    # cabal-install
    libreoffice
    # ghc
    # cabal2nix
    # haskellPackages.hakyll
    zoom-us
    calibre
    # haskellPackages.leksah
    # unstable.vscodium
    haskellPackages.nixfmt
    zsh-nix-shell
    zsh-powerlevel10k

    jdk11
    graphviz

    gimp
  ];

  home.keyboard.options = [ "ctrl:nocaps" ];

  manual.html.enable = true;
  news.display = "silent";

  # Interesting, research more
  # home.file.<name?>.text

  nixpkgs.config = { allowUnfree = true; };
  nixpkgs.config.packageOverrides = pkgs: {
    nur = import (
      builtins.fetchTarball
        "https://github.com/nix-community/NUR/archive/master.tar.gz"
    ) {
      inherit pkgs;
    };

    unstable = import (
      builtins.fetchTarball
        "https://github.com/NixOS/nixpkgs-channels/archive/nixos-unstable.tar.gz"
    ) {
      inherit pkgs;
    };
  };

  programs = {
    # Let Home Manager install and manage itself.
    home-manager.enable = true;

    bat = {
      enable = true;
      config = { pager = "less -FR"; theme = "TwoDark"; };
      themes = {
        dracula = builtins.readFile (pkgs.fetchFromGitHub {
          owner = "dracula";
          repo = "sublime"; # Bat uses sublime syntax for its themes
          rev = "26c57ec282abcaa76e57e055f38432bd827ac34e";
          sha256 = "019hfl4zbn4vm4154hh3bwk6hm7bdxbr1hdww83nabxwjn99ndhv";
        } + "/Dracula.tmTheme");
      };
    };

    /* beets = {
      enable = true;
    }; */

    emacs = {
      enable = true;
      extraPackages = epkgs: [
        epkgs.nix-mode
        epkgs.magit
      ];
    };

    firefox = {
      enable = true;
      extensions = with pkgs.nur.repos.rycee.firefox-addons; [
        https-everywhere
        privacy-badger
        bitwarden
        ublock-origin
        facebook-container
        google-search-link-fix
        old-reddit-redirect
      ];
      profiles = {
        default = {
          isDefault = true;
          settings = {
            "browser.search.suggest.enabled" = false;
            "browser.tabs.closeWindowWithLastTab" = true;
            "devtools.theme" = "dark";
            "browser.urlbar.placeholderName" = "DuckDuckGo";
          };
        };
      };
    };

    git = {
      package = pkgs.gitAndTools.gitFull;
      enable = true;
      userName = "Bill Ewanick";
      userEmail = "bill@ewanick.com";
      aliases = {
        # s = "status";
        # co = "checkout";
      };
      extraConfig = {

      };

      delta.enable = false;
    };

    chromium = {
      enable = true;
      extensions = [
        "cjpalhdlnbpafiamejdnhcphjbkeiagm"
      ];
    };

    qutebrowser = {
      enable = true;
    };

    rofi = {
      enable = true;
    };

    rtorrent = {
      enable = true;
    };

    ssh = {
      enable = true;
      forwardAgent = true;
      matchBlocks = {
        "github.com" = {
          hostname = "github.com";
          user = "git";
          identityFile = [ "${builtins.getEnv "HOME"}/.ssh/id_ed25519" ];
          identitiesOnly = true;
        };
        "gitlab.com" = {
          hostname = "gitlab.com";
          user = "git";
          identityFile = [ "${builtins.getEnv "HOME"}/.ssh/id_gitlab" ];
          identitiesOnly = true;
        };
      };
    };

    starship = {
      enable = true;
      enableZshIntegration = true;
      settings = {
        add_newline = false;
      };
    };

    taskwarrior = {
      enable = true;
    };

    vscode = {
      enable = true;
      package = pkgs.vscodium;
      # package = pkgs.vscode;
      extensions = (with pkgs.vscode-extensions; [
        bbenoist.Nix
        formulahendry.auto-close-tag
        justusadam.language-haskell
      ]) ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [{
        name = "elm-ls-vscode";
        publisher = "Elmtooling";
        version = "0.10.2";
        sha256 = "f64546efabec4f7d83c00f33a4f0d5f7a69d0a53a1bdf388dfebb9791514c59f";
      }
      {
        name = "nix-env-selector";
        publisher = "arrterian";
        version = "0.1.2";
        sha256 = "693371af5b1a51a37d23cd946020ec42f1fd5015a3b9efc14a75263103a7b1d8";
      }
      ];
      userSettings = {

      };
    };

    zathura = {
      enable = true;
      extraConfig = "
        map <C-i> zoom in
        map <C-o> zoom out
      ";
    };

    direnv.enable = true;
    direnv.enableZshIntegration = true;
    direnv.enableNixDirenvIntegration = true;

    zsh = {
      enable = true;
      enableAutosuggestions = true;
      enableCompletion = true;
      autocd = true;
      oh-my-zsh = {
        enable = true;
        plugins = [
          "git"
          "sudo"
          "command-not-found"
          "cp"
          "ssh-agent"
          "node" "npm"
          "themes"
        ];
        theme = "pygmalion";
      };
      shellAliases = {
        # Navigation
        ".." = "cd ..";
        home = "cd ~/workspace";

        # Program enhancement defaults
        cat = "bat";
        nano = "nano -EPcl";
        ls = "ls -lha";

        # Curling APIs
        weather = "curl wttr.in/Ottawa";

        # Laptop Brightness
        brightnessDim  = "xrandr --output eDP1 --brightness 0.1";
        brightnessFull = "xrandr --output eDP1 --brightness 1.0";
        brightnessMid  = "xrandr --output eDP1 --brightness 0.5";

        # Git aliases
        gs = "git status";
        gc = "git commit -a -m";
        gd = "git diff";
        gr = "git reset HEAD .";
        gl = "git log";
        gb = "git branch";
        gco = "git checkout -";
        # git undo, dangerous to repeat!
        # undo = "git reset --soft HEAD^"
      };
      initExtra = ''
        # put this either in bashrc or zshrc
        nixify() {
          if [ ! -e ./.envrc ]; then
            echo "use nix" > .envrc
            direnv allow
          fi
          if [[ ! -e shell.nix ]] && [[ ! -e default.nix ]]; then
            cat > default.nix <<'EOF'
        with import <nixpkgs> {};
        mkShell {
          nativeBuildInputs = [
            bashInteractive
          ];
        }
        EOF
            ${EDITOR:-vim} default.nix
          fi
        }


        # Powerlevel10k
        # https://github.com/evanjs/nixos_cfg/blob/ee9244012e12d8f3d03453de35a0d6e7f14bb4b8/config/new-modules/zsh.nix#L24
        source ${pkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme
        source ~/.p10k.zsh

        # I'm running VSCode inside WSL because it's managed by home-manager
        # To disable the warning from the terminal
        export DONT_PROMPT_WSL_INSTALL="dontPromptMe"

        # https://medium.com/@japheth.yates/the-complete-wsl2-gui-setup-2582828f4577
        # For getting X-Server apps installed with Nix running on Windows 10
        export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0
        export LIBGL_ALWAYS_INDIRECT=1
        sudo /etc/init.d/dbus start &> /dev/null
      '';
    };
  };

  xsession = {
    enable = false;
    numlock.enable = true;
    # windowManager.xmonad = {
    #   enable = false;
    #   enableContribAndExtras = true;
    #   config = pkgs.writeText "xmonad.hs" ''
    #     import XMonad
    #     main = xmonad defaultConfig
    #         { terminal    = "urxvt"
    #         , modMask     = mod4Mask
    #         , borderWidth = 3
    #         }
    #     '';
    # };
  };
}
